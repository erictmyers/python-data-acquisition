# PyDAQ - a simple data acquisition app using python and pyqtgraph
# run from the commandline:
#  python py_daq.py

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import sys,ast,csv,time,os
import pyqtgraph as pg
import numpy as np
import shutil
import hashlib

from PyQt5 import QtWidgets,QtGui,uic
from PyQt5.QtCore import QTimer, QElapsedTimer
from datetime import datetime
from threading import Thread
from collections import deque
from zipfile import ZipFile


class axisChoiceItem(QtWidgets.QComboBox):
    def __init__(self):
        super().__init__()
        self.addItems(["left","right"])
        self.setCurrentIndex(0)

class moduleChoiceItem(QtWidgets.QComboBox):
    def __init__(self):
        super().__init__()
        self.addItems(["GP50_612","USBTC01","dummy_thermocouple","dummy_pressure_sensor"])
        self.setCurrentIndex(3)

class penColorChoiceItem(QtWidgets.QComboBox):
    def __init__(self):
        super().__init__()
        self.addItems(["blue","green","red","cyan","magenta","black","yellow"])
        self.setCurrentIndex(5)

class PyDaq(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.DATA_POINTS_TO_DISPLAY = 200

        self.timer = QElapsedTimer()
        self.timer.start()
        
        uic.loadUi("py_daq_ui.ui",self)

        freq_validator=QtGui.QDoubleValidator()
        freq_validator.setRange( .1, 3600.0, 1)
        self.refreshDelayLineEdit.setValidator(freq_validator)

        self.need_to_save=False

        self.startDataCollectionPushButton.clicked.connect(self.start)
        self.stopDataCollectionPushButton.clicked.connect(self.stop)
        self.loadConfig.clicked.connect(self.load_config)
        self.saveConfig.clicked.connect(self.save_config)
        self.removeSensor.clicked.connect(self.remove_sensor_row)
        self.addSensor.clicked.connect(self.add_sensor_row)

    def remove_sensor_row(self):
        self.sensorTableWidget.removeRow(self.sensorTableWidget.currentRow())

    def add_sensor_row(self):
        row_measurement=QtWidgets.QTableWidgetItem("")
        axis_choice=axisChoiceItem()
        module_choice=moduleChoiceItem()
        row_sensor_ID=QtWidgets.QTableWidgetItem("")
        row_sensor_address=QtWidgets.QTableWidgetItem("")
        pen_color_choice=penColorChoiceItem()
        current_row_count=self.sensorTableWidget.rowCount()
        self.sensorTableWidget.insertRow(current_row_count)

        self.sensorTableWidget.setItem(current_row_count,0,row_measurement)
        self.sensorTableWidget.setCellWidget(current_row_count,1,axis_choice)
        self.sensorTableWidget.setCellWidget(current_row_count,2,module_choice)
        self.sensorTableWidget.setItem(current_row_count,3,row_sensor_ID)
        self.sensorTableWidget.setItem(current_row_count,4,row_sensor_address)
        self.sensorTableWidget.setCellWidget(current_row_count,5,pen_color_choice)

    def config_saver(self,filepath):
        test_init={}
        
        test_init['test_description']=self.testDescLineEdit.text() 
        
        frequency=float(self.refreshDelayLineEdit.text())
        test_init['refresh_delay']=int(frequency*1000) # delay is in ms
        
        test_init['left_Y_min']=float(self.leftYMinLineEdit.text())
        test_init['left_Y_max']=float(self.leftYMaxLineEdit.text())
        test_init['left_Y_label']=self.leftYLabel.text()
        
        test_init['right_Y_min']=float(self.rightYMinLineEdit.text())
        test_init['right_Y_max']=float(self.rightYMaxLineEdit.text())
        test_init['right_Y_label']=self.rightYLabel.text()

        sensor_list=[]
        num_sensors=self.sensorTableWidget.rowCount()

        for current_row_count in range(num_sensors):
            sensor={}
            sensor['label']=self.sensorTableWidget.item(current_row_count,0).text()
            sensor['y_axis']=self.sensorTableWidget.cellWidget(current_row_count,1).currentText()
            sensor['module']=self.sensorTableWidget.cellWidget(current_row_count,2).currentText()
            sensor['ID']=self.sensorTableWidget.item(current_row_count,3).text()
            sensor['address']=self.sensorTableWidget.item(current_row_count,4).text()
            sensor['pen_color']=self.sensorTableWidget.cellWidget(current_row_count,5).currentText()
            sensor_list.append(sensor)

        test_init['sensor_list']=sensor_list

        with open(filepath,'w') as f:
            print(test_init,file=f)

    def save_config(self):
        filepath = QtWidgets.QFileDialog.getSaveFileName(self,
            "Save Configuration File", "./", "Configuration Files (*.dct)")
        self.config_saver(filepath[0])

    def load_config(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(self,
            "Load Configuration File", "./", "Configuration Files (*.dct)")

        with open(fileName[0]) as f:
            data = f.read()
        test_init = ast.literal_eval(data)

        self.testDescLineEdit.setText(test_init['test_description'])
        print("Test description is: "+self.testDescLineEdit.text())

        frequency=float(test_init['refresh_delay'])/1000
        self.refreshDelayLineEdit.setText(str(frequency))

        left_Y_min=test_init['left_Y_min']        
        self.leftYMinLineEdit.setText(str(left_Y_min))
        
        left_Y_max=test_init['left_Y_max']
        self.leftYMaxLineEdit.setText(str(left_Y_max))
        
        left_Y_label=test_init['left_Y_label']
        self.leftYLabel.setText(left_Y_label)

        right_Y_min=test_init['right_Y_min']
        self.rightYMinLineEdit.setText(str(right_Y_min))
        
        right_Y_max=test_init['right_Y_max']
        self.rightYMaxLineEdit.setText(str(right_Y_max))
        
        right_Y_label=test_init['right_Y_label']
        self.rightYLabel.setText(right_Y_label)

        self.sensorTableWidget.setRowCount(0) # clear any existing rows from table
        sensor_list=test_init["sensor_list"]

        for sensor in sensor_list:
            row_measurement=QtWidgets.QTableWidgetItem(sensor['label'])

            axis_choice=axisChoiceItem()
            if sensor['y_axis']=="right":
                axis_choice.setCurrentIndex(1)

            module_choice=moduleChoiceItem()
            if sensor['module']=="GP50_612":
                module_choice.setCurrentIndex(0)
            elif sensor['module']=="USBTC01":
                module_choice.setCurrentIndex(1)
            elif sensor['module']=="dummy_thermocouple":
                module_choice.setCurrentIndex(2)

            row_sensor_ID=QtWidgets.QTableWidgetItem(sensor['ID'])
            
            row_sensor_address=QtWidgets.QTableWidgetItem(sensor['address'])

            pen_color_choice=penColorChoiceItem()
            if sensor['pen_color']=="blue":
                pen_color_choice.setCurrentIndex(0)
            elif sensor['pen_color']=="green":
                pen_color_choice.setCurrentIndex(1)
            elif sensor['pen_color']=="red":
                pen_color_choice.setCurrentIndex(2)
            elif sensor['pen_color']=="cyan":
                pen_color_choice.setCurrentIndex(3)
            elif sensor['pen_color']=="magenta":
                pen_color_choice.setCurrentIndex(4)
            elif sensor['pen_color']=="black":
                pen_color_choice.setCurrentIndex(5)
            elif sensor['pen_color']=="yellow":
                pen_color_choice.setCurrentIndex(6)

            current_row_count=self.sensorTableWidget.rowCount()
            self.sensorTableWidget.insertRow(current_row_count)
            self.sensorTableWidget.setItem(current_row_count,0,row_measurement)
            self.sensorTableWidget.setCellWidget(current_row_count,1,axis_choice)
            self.sensorTableWidget.setCellWidget(current_row_count,2,module_choice)
            self.sensorTableWidget.setItem(current_row_count,3,row_sensor_ID)
            self.sensorTableWidget.setItem(current_row_count,4,row_sensor_address)
            self.sensorTableWidget.setCellWidget(current_row_count,5,pen_color_choice)

    def initialize_plot(self):
        # Data vs time plot
        self.plot_widget = pg.PlotWidget(axisItems={'bottom': pg.DateAxisItem(orientation='bottom')})
        self.plot_widget.setBackground("w")

        # Enable/disable plot squeeze (Fixed axis movement)
        self.plot_widget.plotItem.setMouseEnabled(x=False, y=False)
        self.plot_widget.setTitle(self.testDescLineEdit.text())
        self.plot_widget.setLabel('left', self.leftYLabel.text())
        self.plot_widget.setLabel('right', self.rightYLabel.text())
        self.plot_widget.setLabel('bottom', 'Time')

        self.plot_widget.addLegend()
        self.plot_widget.showGrid(x=True, y=True)

        self.right_vb = pg.ViewBox()
        self.right_plot_item = self.plot_widget.plotItem
        self.right_plot_item.showAxis('right')
        self.right_plot_item.scene().addItem(self.right_vb)
        self.right_plot_item.getAxis('right').linkToView(self.right_vb)
        self.right_vb.setXLink(self.right_plot_item)

        self.update_views()
        self.right_plot_item.vb.sigResized.connect(self.update_views)

        right_Y_min=float( self.rightYMinLineEdit.text() )
        right_Y_max=float( self.rightYMaxLineEdit.text() )
        self.right_vb.setYRange(right_Y_min,right_Y_max)

        left_Y_min=float( self.leftYMinLineEdit.text() )
        left_Y_max=float( self.leftYMaxLineEdit.text() )
        self.plot_widget.setYRange(left_Y_min,left_Y_max)

        num_sensors=self.sensorTableWidget.rowCount()
        self.sensor_objects=[]

        for current_row_count in range(num_sensors):
            sensor={}
            sensor['label']=self.sensorTableWidget.item(current_row_count,0).text()
            sensor['y_axis']=self.sensorTableWidget.cellWidget(current_row_count,1).currentText()
            sensor['module']=self.sensorTableWidget.cellWidget(current_row_count,2).currentText()
            sensor['ID']=self.sensorTableWidget.item(current_row_count,3).text()
            sensor['address']=self.sensorTableWidget.item(current_row_count,4).text()
            sensor['pen_color']=self.sensorTableWidget.cellWidget(current_row_count,5).currentText()

            sensor_object=self.get_sensor(sensor)
            sensor_object.create_connection()
            sensor_pen=pg.mkPen(sensor_object.pen_color, width=2)
            sensor_object.plot_line=pg.PlotCurveItem([], [], pen=sensor_pen, name=sensor_object.label)

            if sensor_object.y_axis=="left":
                self.plot_widget.plotItem.addItem(sensor_object.plot_line)
            else:
                self.right_vb.addItem(sensor_object.plot_line)
                self.plot_widget.plotItem.legend.addItem(sensor_object.plot_line, sensor_object.plot_line.name())

            sensor_object.data = deque(maxlen=self.DATA_POINTS_TO_DISPLAY)
            self.sensor_objects.append(sensor_object)
            
        # Remove any leftover plots first
        for i in reversed(range(self.plotGridLayout.count())): 
            self.plotGridLayout.itemAt(i).widget().setParent(None)

        self.plotGridLayout.addWidget(self.plot_widget)


    def get_sensor(self,sensor):
        if sensor['module']=="USBTC01":
            from sensors.usb_tc01 import USBTC01
            sensor_object=USBTC01()
        elif sensor['module']=="GP50_612":
            from sensors.gp50_612 import GP50_612
            sensor_object=GP50_612()
        elif sensor['module']=="dummy_thermocouple":
            from sensors.dummy_temperature import DummyTemp
            sensor_object=DummyTemp()
        elif sensor['module']=="dummy_pressure_sensor":
            from sensors.dummy_pressure import DummyPress
            sensor_object=DummyPress()
        else:
            sensor_object=1

        sensor_object.address=sensor['address']
        sensor_object.ID=sensor['ID']
        sensor_object.label=sensor['label']
        sensor_object.y_axis=sensor['y_axis']
        # Good options for color are blue, green, red, blue, cyan, magenta, black
        sensor_object.pen_color=sensor['pen_color']

        return sensor_object

    def start(self):
        """Begin plotting"""

        self.initialize_plot()

        for sensor_object in self.sensor_objects:
            sensor_object.data.clear()

        self.startDataCollectionPushButton.setEnabled(False)
        self.loadConfig.setEnabled(False)
        self.saveConfig.setEnabled(False)
        self.addSensor.setEnabled(False)
        self.removeSensor.setEnabled(False)
        self.sensorTableWidget.setEnabled(False)

        self.stopDataCollectionPushButton.setEnabled(True)
        
        self.FREQUENCY=float(self.refreshDelayLineEdit.text())
        self.SCROLLING_TIMESTAMP_PLOT_REFRESH_RATE=int(self.FREQUENCY*1000) # delay is in ms

        self.collection_running=True
        self.read_position_thread()

        test_id=datetime.now().isoformat()
        self.test_id=test_id.replace(":","-")
        os.mkdir(self.test_id)
        init_record_filename=self.test_id+"_init.dct"
        self.init_record_file_path=os.path.join(self.test_id,init_record_filename)
        self.config_saver(self.init_record_file_path)
        # shutil.copyfile('test_init.dct', self.init_record_file_path)

        self.dataSetIdLabel.setText(self.test_id)

        fieldnames=['Time',]
        for sensor_object in self.sensor_objects:
            fieldnames.append(sensor_object.label)

        csv_filename=self.test_id+".csv"
        self.csv_file_path=os.path.join(self.test_id,csv_filename)
        with open(self.csv_file_path, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

        self.need_to_save=True
        
        self.position_update_timer = QTimer()
        self.position_update_timer.timeout.connect(self.plot_updater)
        self.position_update_timer.start(self.SCROLLING_TIMESTAMP_PLOT_REFRESH_RATE)

    def closeEvent(self, event):
        if self.need_to_save:
            self.stop()

    def stop(self):
    ## !!!! Make sure current configuration is saved !!!!!!

        self.startDataCollectionPushButton.setEnabled(True)
        self.loadConfig.setEnabled(True)
        self.saveConfig.setEnabled(True)
        self.addSensor.setEnabled(True)
        self.removeSensor.setEnabled(True)
        self.sensorTableWidget.setEnabled(True)

        self.stopDataCollectionPushButton.setEnabled(False)

        self.position_update_timer.stop()
        self.collection_running=False

        zip_name=self.test_id+".zip"
        zip_file_path=os.path.join(self.test_id,zip_name)
        with ZipFile(zip_file_path, 'w') as zip_file:
            zip_file.write(self.init_record_file_path)
            zip_file.write(self.csv_file_path)

        with open(zip_file_path, 'rb') as file_obj:
            file_contents = file_obj.read()
            md5_hash = hashlib.md5(file_contents).hexdigest()

        hash_filename=self.test_id+"_checksum.md5"
        hash_file_path=os.path.join(self.test_id,hash_filename)
        with open(hash_file_path, 'w') as f:
            f.write(md5_hash)

        self.need_to_save=False

    def update_views(self):
        self.right_vb.setGeometry(self.right_plot_item.vb.sceneBoundingRect())
        self.right_vb.linkedViewChanged(self.right_plot_item.vb, self.right_vb.XAxis)

    def read_position_thread(self):
        """Read in data using a thread"""
        self.current_position_value = 0
        self.position_update_thread = Thread(target=self.read_position, args=())
        self.position_update_thread.daemon = True
        self.position_update_thread.start()

    def read_position(self):
        frequency=self.FREQUENCY
        while self.collection_running:
            print(time.time())
            for sensor_object in self.sensor_objects:
                sensor_data=sensor_object.sensor_read()
                print(sensor_data)
                sensor_object.current_position_value = sensor_data
            print("\n")

            time.sleep(frequency)

    def plot_updater(self):
        x=int(time.time())

        fieldnames=['Time',]
        data_dict={'Time':x}

        for sensor_object in self.sensor_objects:
            data_point = float(sensor_object.current_position_value)
            sensor_object.data.append({'x':int(time.time()),'y':data_point})
            sensor_object.plot_line.setData(x=[item['x'] for item in sensor_object.data], y=[item['y'] for item in sensor_object.data])
            key=sensor_object.label
            fieldnames.append(key)
            data_dict[key]=data_point

        with open(self.csv_file_path,  'a+', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow(data_dict)

class main():
    if sys.version_info[0] < 3:
        print("Requires Python 3")
        sys.exit()
    a=QtWidgets.QApplication(sys.argv)
    w=PyDaq()
    w.show()
    sys.exit(a.exec_())

if __name__=='__main__':
    if sys.version_info[0] < 3:
        print("Requires Python 3")
        sys.exit()
    a=QtWidgets.QApplication(sys.argv)
    w=PyDaq()
    w.show()
    sys.exit(a.exec_())
