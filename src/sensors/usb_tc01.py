# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import nidaqmx

class USBTC01():
    def __init__(self,address="Dev1/ai0",ID="xxx",label="Temperature",unit_label="deg F",
                model="NI USB-TC01, Type K thermocouple",min_val=0.0,max_val=200.0,
                units=nidaqmx.constants.TemperatureUnits.DEG_F,thermocouple_type=nidaqmx.constants.ThermocoupleType.K,
                cjc_source=nidaqmx.constants.CJCSource.BUILT_IN):":
                
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label=unit_label
        self.model=model
        self.min_val=min_val
        self.max_val=max_val
        self.units=units
        self.thermocouple_type=thermocouple_type
        self.cjc_source=cjc_source

        self.task=nidaqmx.Task()

    def create_connection(self):
        self.task.ai_channels.add_ai_thrmcpl_chan(self.address,self.label,self.min_val,self.max_val,
                self.units,self.thermocouple_type,self.cjc_source)

    def sensor_read(self):
        return self.task.read()