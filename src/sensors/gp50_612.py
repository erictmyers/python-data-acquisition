# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import serial

class GP50_612():
    def __init__(self,address="COM4",ID="xxx",label="Pressure",unit_label="psi",model="GP:50 612",baud_rate=115200,timeout=2,):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label=unit_label
        self.model=model
        self.baud_rate=baud_rate
        self.timeout=timeout

    def create_connection(self):
        self.conn=serial.Serial(self.address, self.baud_rate, timeout=self.timeout)

    def sensor_read(self):
        self.conn.write(("!001:SYS?\r").encode())
        data=self.conn.read_until(expected=b'\r')
        return data.decode().strip()