# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import random

class DummyPress():
    def __init__(self,address="address",ID="xxx",label="Pressure",):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label="psi"
        self.model="Dummy pressure sensor"

    def create_connection(self):
        pass

    def sensor_read(self):
        return random.randint(2500,3500)