# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import random

class DummyTemp():
    def __init__(self,address="address",ID="xxx",label="Temperature",):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label="deg F"
        self.model="Dummy thermocouple"

    def create_connection(self):
        pass

    def sensor_read(self):
        return random.randint(70,95)