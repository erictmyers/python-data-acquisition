import sys,ast,csv,time
import os.path
import pyqtgraph as pg
import pyqtgraph.exporters

from docx import Document
from PyQt5 import QtWidgets,QtGui,uic

class axisChoiceItem(QtWidgets.QComboBox):
    def __init__(self):
        super().__init__()
        self.addItems(["left","right"])
        self.setCurrentIndex(0)

class moduleChoiceItem(QtWidgets.QComboBox):
    def __init__(self):
        super().__init__()
        self.addItems(["GP50_612","USBTC01","dummy_thermocouple","dummy_pressure_sensor"])
        self.setCurrentIndex(3)

class penColorChoiceItem(QtWidgets.QComboBox):
    def __init__(self):
        super().__init__()
        self.addItems(["blue","green","red","cyan","magenta","black","yellow"])
        self.setCurrentIndex(5)

class Sensor(object):
    pass

class ReportGenerator(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        uic.loadUi("report_generator_ui.ui",self)

        self.browse_directory=os.path.dirname(os.path.abspath(sys.argv[0]))

        self.loadCollectedDataPushButton.clicked.connect(self.load_data)
        self.updatePlotPushButton.clicked.connect(self.plot_updater)
        self.loadConfigPushButton.clicked.connect(self.load_config)
        self.saveConfigPushButton.clicked.connect(self.save_config)
        self.generateReportPushButton.clicked.connect(self.generate_report)

    def replace_text(self,paragraph,key,value):
        if key in paragraph.text:
            inline = paragraph.runs
            for item in inline:
                if key in item.text:
                    item.text = item.text.replace(key, value)

    def generate_report(self):
        template = QtWidgets.QFileDialog.getOpenFileName(self,
            caption="Select Report Template", directory=self.browse_directory, filter="Report Template Document (*.docx)")[0]
        self.browse_directory=os.path.dirname(template)

        template_document = Document(template)

        replacements_list=[
            ["$globalTestDesc",self.globalTestDescLineEdit.text()],
            ["$globalTestProc",self.testProcedureLineEdit.text()],
            ["$globalEquipmentPn",self.equipmentPNLineEdit.text()],
            ["$globalEquipmentSn",self.equipmentSNLineEdit.text()],
            ["$globalEquipmentDesc",self.equipmentDescLineEdit.text()],
            ["$testDescription",self.testDescLineEdit.text()],
            ["$testDate",self.testDateLineEdit.text()],
            ["$testerName",self.testerNameLineEdit.text()],
            ["$commentsNotes",self.commentsTextEdit.toPlainText()]
        ]

        for replacement_pair in replacements_list:
            for para in template_document.paragraphs:
                self.replace_text(para,replacement_pair[0],replacement_pair[1])

        exporter = pg.exporters.ImageExporter(self.plot_widget.plotItem)
        exporter.export('temp.png')
        template_document.add_picture('temp.png')

        sensors_table=template_document.tables[0]

        num_sensors=self.sensorTableWidget.rowCount()
        for current_row_count in range(num_sensors):
            row=sensors_table.add_row().cells
            row[0].text = self.sensorTableWidget.item(current_row_count,0).text() # "measurement"
            row[1].text = self.sensorTableWidget.item(current_row_count,1).text() # "ID"
            row[2].text = self.sensorTableWidget.cellWidget(current_row_count,2).currentText() # "mfg"
            row[3].text = self.sensorTableWidget.item(current_row_count,3).text() # "units"

        sensors_table.style=="Table Grid"

        filepath = QtWidgets.QFileDialog.getSaveFileName(self,
            caption="Save Report", directory=self.browse_directory, filter="Word Documents (*.docx)")[0]
        self.browse_directory=os.path.dirname(filepath)
        template_document.save(filepath)

    def load_config(self):
        config_filename = QtWidgets.QFileDialog.getOpenFileName(self,
            caption="Load Global Configuration", directory=self.browse_directory, filter="Global Configuration Files (*.global)")[0]
        self.browse_directory=os.path.dirname(config_filename)


        with open(config_filename) as f:
            data = f.read()
        global_init = ast.literal_eval(data)

        self.globalTestDescLineEdit.setText(global_init['global_test_desc'])
        self.testProcedureLineEdit.setText(global_init['global_test_proc'])
        self.equipmentPNLineEdit.setText(global_init['global_equipment_pn'])
        self.equipmentDescLineEdit.setText(global_init['global_equipment_desc'])
        self.equipmentSNLineEdit.setText(global_init['global_equipment_sn'])

    def save_config(self):
        filepath = QtWidgets.QFileDialog.getSaveFileName(self,
            caption="Save Global Configuration", directory=self.browse_directory, filter="Global Configuration Files (*.global)")[0]
        self.browse_directory=os.path.dirname(filepath)

        global_init={}

        global_init['global_test_desc']=self.globalTestDescLineEdit.text()
        global_init['global_test_proc']=self.testProcedureLineEdit.text()
        global_init['global_equipment_pn']=self.equipmentPNLineEdit.text()
        global_init['global_equipment_desc']=self.equipmentDescLineEdit.text()
        global_init['global_equipment_sn']=self.equipmentSNLineEdit.text()

        with open(filepath,'w') as f:
            print(global_init,file=f)

    def load_data(self):
        config_filename = QtWidgets.QFileDialog.getOpenFileName(self,
            caption="Load Data Files", directory=self.browse_directory, filter="Configuration Files (*.dct)")[0]
        self.browse_directory=os.path.dirname(config_filename)
        # print("config_filename is "+config_filename)

        with open(config_filename) as f:
            data = f.read()
        test_init = ast.literal_eval(data)

        self.testDescLineEdit.setText(test_init['test_description'])
        # print("Test description is: "+self.testDescLineEdit.text())

        # left_Y_min=test_init['left_Y_min']
        # self.leftYMinLineEdit.setText(str(left_Y_min))

        # left_Y_max=test_init['left_Y_max']
        # self.leftYMaxLineEdit.setText(str(left_Y_max))

        left_Y_label=test_init['left_Y_label']
        self.leftYLabel.setText(left_Y_label)

        # right_Y_min=test_init['right_Y_min']
        # self.rightYMinLineEdit.setText(str(right_Y_min))

        # right_Y_max=test_init['right_Y_max']
        # self.rightYMaxLineEdit.setText(str(right_Y_max))

        right_Y_label=test_init['right_Y_label']
        self.rightYLabel.setText(right_Y_label)

        self.sensorTableWidget.setRowCount(0) # clear any existing rows from table
        sensor_list=test_init["sensor_list"]

        for sensor in sensor_list:
            row_measurement=QtWidgets.QTableWidgetItem(sensor['label'])

            row_sensor_ID=QtWidgets.QTableWidgetItem(sensor['ID'])

            module_choice=moduleChoiceItem()
            if sensor['module']=="GP50_612":
                module_choice.setCurrentIndex(0)
            elif sensor['module']=="USBTC01":
                module_choice.setCurrentIndex(1)
            elif sensor['module']=="dummy_thermocouple":
                module_choice.setCurrentIndex(2)

            row_units=QtWidgets.QTableWidgetItem("")

            axis_choice=axisChoiceItem()
            if sensor['y_axis']=="right":
                axis_choice.setCurrentIndex(1)

            pen_color_choice=penColorChoiceItem()
            if sensor['pen_color']=="blue":
                pen_color_choice.setCurrentIndex(0)
            elif sensor['pen_color']=="green":
                pen_color_choice.setCurrentIndex(1)
            elif sensor['pen_color']=="red":
                pen_color_choice.setCurrentIndex(2)
            elif sensor['pen_color']=="cyan":
                pen_color_choice.setCurrentIndex(3)
            elif sensor['pen_color']=="magenta":
                pen_color_choice.setCurrentIndex(4)
            elif sensor['pen_color']=="black":
                pen_color_choice.setCurrentIndex(5)
            elif sensor['pen_color']=="yellow":
                pen_color_choice.setCurrentIndex(6)

            current_row_count=self.sensorTableWidget.rowCount()
            self.sensorTableWidget.insertRow(current_row_count)
            self.sensorTableWidget.setItem(current_row_count,0,row_measurement)
            self.sensorTableWidget.setItem(current_row_count,1,row_sensor_ID)
            self.sensorTableWidget.setCellWidget(current_row_count,2,module_choice)
            self.sensorTableWidget.setItem(current_row_count,3,row_units)
            self.sensorTableWidget.setCellWidget(current_row_count,4,axis_choice)
            self.sensorTableWidget.setCellWidget(current_row_count,5,pen_color_choice)

        file_base_name=os.path.basename(config_filename)
#        print("file_base_name is "+file_base_name)
        test_date=file_base_name.split("T")[0]
        self.testDateLineEdit.setText(test_date)
        data_file_directory=os.path.dirname(config_filename)
#        print("data_file_directory is "+data_file_directory)
        csv_file_basename=file_base_name.strip("_init.dct")+".csv"
#        print("csv_file_basename is "+csv_file_basename)
        data_filename=os.path.join(data_file_directory,csv_file_basename)
#        print("data_filename is "+data_filename)

        # Clear any existing data from the table.
        self.dataTableWidget.clear()
        self.dataTableWidget.setRowCount(0)
        self.dataTableWidget.setColumnCount(0)

        data=[]
        with open(data_filename, 'r') as csvfile:
            reader = csv.reader(csvfile)
            for line in reader:
                data.append(line)
        data_headers=data[0]
        self.dataTableWidget.setColumnCount(len(data_headers))
        self.dataTableWidget.setHorizontalHeaderLabels(data_headers)
        for row in data[1:]:
            current_row_count=self.dataTableWidget.rowCount()
            self.dataTableWidget.insertRow(current_row_count)
            for i in range(len(row)):
                data=row[i]
                # if i==0: #Convert time stamp to human-readable time
                    # time_obj=time.localtime(int(row[i]))
                    # data=str(time_obj.tm_hour)+":"+str(time_obj.tm_min)+":"+str(time_obj.tm_sec)
                # else:
                    # data=row[i]
                table_item=QtWidgets.QTableWidgetItem(data)
                self.dataTableWidget.setItem(current_row_count,i,table_item)

        self.generateReportPushButton.setEnabled(True)
        self.plot_updater()

    def update_views(self):
        self.right_vb.setGeometry(self.right_plot_item.vb.sceneBoundingRect())
        self.right_vb.linkedViewChanged(self.right_plot_item.vb, self.right_vb.XAxis)

    def plot_updater(self):
        self.plot_widget = pg.PlotWidget(axisItems={'bottom': pg.DateAxisItem(orientation='bottom')})
        self.plot_widget.setBackground("w")
        self.plot_widget.setTitle(self.testDescLineEdit.text())
        self.plot_widget.setLabel('left', self.leftYLabel.text())
        self.plot_widget.setLabel('right', self.rightYLabel.text())
        self.plot_widget.setLabel('bottom', 'Time')

        self.plot_widget.addLegend()
        self.plot_widget.showGrid(x=True, y=True)

        self.right_vb = pg.ViewBox()
        self.right_plot_item = self.plot_widget.plotItem
        self.right_plot_item.showAxis('right')
        self.right_plot_item.scene().addItem(self.right_vb)
        self.right_plot_item.getAxis('right').linkToView(self.right_vb)
        self.right_vb.setXLink(self.right_plot_item)

        self.update_views()
        self.right_plot_item.vb.sigResized.connect(self.update_views)


        # right_Y_min=float( self.rightYMinLineEdit.text() )
        # right_Y_max=float( self.rightYMaxLineEdit.text() )
        # self.right_vb.setYRange(right_Y_min,right_Y_max)

        # left_Y_min=float( self.leftYMinLineEdit.text() )
        # left_Y_max=float( self.leftYMaxLineEdit.text() )
        # self.plot_widget.setYRange(left_Y_min,left_Y_max)


        num_sensors=self.sensorTableWidget.rowCount()
        self.sensor_objects=[]

        # print("Number of sensors is "+str(num_sensors))

        for current_row_count in range(num_sensors):
            sensor_object=Sensor()
            sensor_object.label=self.sensorTableWidget.item(current_row_count,0).text()

            for col_num in range(self.dataTableWidget.columnCount()):
                if self.dataTableWidget.horizontalHeaderItem(col_num).text()==sensor_object.label:
                    sensor_object.data_col_num=col_num

            sensor_object.y_axis=self.sensorTableWidget.cellWidget(current_row_count,4).currentText()
            sensor_object.pen_color=self.sensorTableWidget.cellWidget(current_row_count,5).currentText()

            sensor_pen=pg.mkPen(sensor_object.pen_color, width=2)
            sensor_object.plot_line=pg.PlotCurveItem([], [], pen=sensor_pen, name=sensor_object.label)

            if sensor_object.y_axis=="left":
                self.plot_widget.plotItem.addItem(sensor_object.plot_line)
            else:
                self.right_vb.addItem(sensor_object.plot_line)
                self.plot_widget.plotItem.legend.addItem(sensor_object.plot_line, sensor_object.plot_line.name())

#            sensor_object.data = deque(maxlen=self.DATA_POINTS_TO_DISPLAY)
            sensor_object.data=[]

            self.sensor_objects.append(sensor_object)

        # for sensor_object in self.sensor_objects:
            # print("\n Measurement: "+sensor_object.label)

        for row_num in range(self.dataTableWidget.rowCount()):
            data_time=float(self.dataTableWidget.item(row_num, 0).text())
            for sensor_object in self.sensor_objects:
                data_point=float(self.dataTableWidget.item(row_num, sensor_object.data_col_num).text())
                sensor_object.data.append({'x':data_time,'y':data_point})
                sensor_object.plot_line.setData(x=[item['x'] for item in sensor_object.data], y=[item['y'] for item in sensor_object.data])


        # x=int(time.time())

        # fieldnames=['Time',]
        # data_dict={'Time':x}

        # for sensor_object in self.sensor_objects:
            # data_point = float(sensor_object.current_position_value)
            # sensor_object.data.append({'x':int(time.time()),'y':data_point})
            # sensor_object.plot_line.setData(x=[item['x'] for item in sensor_object.data], y=[item['y'] for item in sensor_object.data])
            # key=sensor_object.label
            # fieldnames.append(key)
            # data_dict[key]=data_point


        # Remove any leftover plots first
        for i in reversed(range(self.plotGridLayout.count())):
            self.plotGridLayout.itemAt(i).widget().setParent(None)

        self.plotGridLayout.addWidget(self.plot_widget)



if __name__=='__main__':
    if sys.version_info[0] < 3:
        print("Requires Python 3")
        sys.exit()
    a=QtWidgets.QApplication(sys.argv)
    w=ReportGenerator()
    w.show()
    sys.exit(a.exec_())
