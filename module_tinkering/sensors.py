# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

import serial
import nidaqmx
import random


class USBTC01():
    def __init__(self,address="Dev1/ai0",ID="xxx",label="Temperature",unit_label="deg F",
                model="NI USB-TC01, Type K thermocouple",min_val=0.0,max_val=200.0,
                units=nidaqmx.constants.TemperatureUnits.DEG_F,thermocouple_type=nidaqmx.constants.ThermocoupleType.K,
                cjc_source=nidaqmx.constants.CJCSource.BUILT_IN):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label=unit_label
        self.model=model
        self.min_val=min_val
        self.max_val=max_val
        self.units=units
        self.thermocouple_type=thermocouple_type
        self.cjc_source=cjc_source

        self.task=nidaqmx.Task()

    def create_connection(self):
        self.task.ai_channels.add_ai_thrmcpl_chan(self.address,self.label,self.min_val,self.max_val,
                self.units,self.thermocouple_type,self.cjc_source)

    def sensor_read(self):
        return self.task.read()

class GP50_612():
    def __init__(self,address="COM4",ID="xxx",label="Pressure",unit_label="psi",model="GP:50 612",baud_rate=115200,timeout=2,):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label=unit_label
        self.model=model
        self.baud_rate=baud_rate
        self.timeout=timeout

    def create_connection(self):
        self.conn=serial.Serial(self.address, self.baud_rate, timeout=self.timeout)

    def sensor_read(self):
        self.conn.write(("!001:SYS?\r").encode())
        data=self.conn.read_until(expected=b'\r')
        return data.decode().strip()

class DummyTemp():
    def __init__(self,address="address",ID="xxx",label="Temperature",):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label="deg F"
        self.model="Dummy thermocouple"

    def create_connection(self):
        pass

    def sensor_read(self):
        return random.randint(70,95)

class DummyPress():
    def __init__(self,address="address",ID="xxx",label="Pressure",):
        self.address=address
        self.ID=ID
        self.label=label
        self.unit_label="psi"
        self.model="Dummy pressure sensor"

    def create_connection(self):
        pass

    def sensor_read(self):
        return random.randint(2500,3500)

def get_sensor(sensor):
    if sensor['module']=="USBTC01":
        sensor_object=USBTC01()
    elif sensor['module']=="GP50_612":
        sensor_object=GP50_612()
    elif sensor['module']=="dummy_thermocouple":
        sensor_object=DummyTemp()
    elif sensor['module']=="dummy_pressure_sensor":
        sensor_object=DummyPress()
    else:
        sensor_object=1

    sensor_object.address=sensor['address']
    sensor_object.ID=sensor['ID']
    sensor_object.label=sensor['label']
    sensor_object.y_axis=sensor['y_axis']
    # Good options for color are blue, green, red, blue, cyan, magenta, black
    sensor_object.pen_color=sensor['pen_color']

    return sensor_object
