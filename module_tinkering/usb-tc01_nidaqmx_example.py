import nidaqmx
from nidaqmx.constants import TerminalConfiguration
import time
import threading
import keyboard

from datetime import datetime

start: bool = False
period: float = 0.1 # every 100 ms ??
logTimeInMinutes: int = 1

def measure_thread(taskDev, devName):

    now = datetime.now()
    fileName = now.strftime("Thermal_Log_%d_%m_%Y_%H_%M_%S")
    print(fileName)
    directory = "Log/"
    fileHandle = open(directory + fileName + ".csv", "w")
    fileHandle.write("TimeStamp (ms), " + "TotalMinElapsed (min), "
                     + "Temperature (F) " + "\n")

    global start
    global period

    startTimeInMs = time.time()*1000
    lastMinElapsed = 0


    while start:
        # timeStamp = now.strftime("%d:%m:%Y:%H:%M:%S , ")
        timeStamp = time.time()*1000 - startTimeInMs
        # 60 seconds in a minute, 1000 ms in a second
        TotalMinElapsed = timeStamp/(1000*60)

        #in Hours
        SegmentHourElapsed = (TotalMinElapsed - lastMinElapsed)/60


        samples_per_channel = 10

        data = taskDev.read(samples_per_channel)
        print(f"{data}")

        SampleAvg = sum(data)/len(data)

        print(f"{SampleAvg}")
        strToWrite = (f"{timeStamp:.3f}, {TotalMinElapsed:.6f}, {SampleAvg:.3f},")

        fileHandle.write(strToWrite + "\n")
        time.sleep(period)
    fileHandle.close()

def main():
    global start

    devName = 'Dev3'

    with nidaqmx.Task() as ThermoTaskDev:
        ThermoChannel = ThermoTaskDev.ai_channels.add_ai_thrmcpl_chan("Dev1/ai0",
                                             name_to_assign_to_channel="Thermocouple",
                                             min_val=0.0,
                                             max_val=200.0, units=nidaqmx.constants.TemperatureUnits.DEG_F,
                                             thermocouple_type=nidaqmx.constants.ThermocoupleType.K,
                                             cjc_source=nidaqmx.constants.CJCSource.BUILT_IN)

        while start is False:
            value = input("Enter 's' to Start: ")
            if (value == 's'):
                start = True

        threadHandle = threading.Thread(
            target=measure_thread, args=(ThermoTaskDev, devName))
        threadHandle.start()

        while True:
            if keyboard.read_key() == "q":
                start = False
                break

        threadHandle.join()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()