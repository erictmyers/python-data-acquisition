import sys
from PyQt5 import QtWidgets,uic
import pyqtgraph as pg

class GraphTester(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        uic.loadUi("pyqt_graph_testing.ui",self)

        # Temperature vs time plot
        self.plot_graph = pg.PlotWidget()
        self.plot_graph.setBackground("w")
        self.gridLayout_2.addWidget(self.plot_graph)
        time = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        temperature = [30, 32, 34, 32, 33, 31, 29, 32, 35, 30]
        self.plot_graph.plot(time, temperature)
        
if __name__=='__main__':
    if sys.version_info[0] < 3:
        print("Requires Python 3")
        sys.exit()
    a=QtWidgets.QApplication(sys.argv)
    w=GraphTester()
    w.show()
    sys.exit(a.exec_())
    
