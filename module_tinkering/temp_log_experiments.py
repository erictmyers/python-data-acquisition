import nidaqmx

with nidaqmx.Task() as task:
    task.ai_channels.add_ai_thrmcpl_chan("Dev1/ai0",name_to_assign_to_channel="thermocouple", min_val=0.0,
                                     max_val=200.0, units=nidaqmx.constants.TemperatureUnits.DEG_F,
                                     thermocouple_type=nidaqmx.constants.ThermocoupleType.K,
                                     cjc_source=nidaqmx.constants.CJCSource.BUILT_IN)
    task.read()