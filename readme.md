## PyDAQ - a simple data acquisition app using python and pyqtgraph

run from the commandline:
`python py_daq.py`

![PyDAQ screenshot](/images/screenshot2.png "PyDAQ screenshot")

#### FEATURES/OPERATION

Prior to starting data acquisition, fill in all fields. Add or remove sensor rows to the table, and set each cell to the required value.

Alternatively, a previously saved configuration file can be loaded to populate the fields and sensor table. 

Sample configuration file contents:
   {"test_description":"Dummy Test #1",
   "refresh_delay":1500,
   "expected_duration":180,
   "left_Y_min":0,
   "left_Y_max":5000,
   "left_Y_label":"Pressure (psi)",
   "right_Y_min":0,
   "right_Y_max":250,
   "right_Y_label":"Temperature (deg F)",
   "sensor_list":[
       {"module":"GP50_612" , "y_axis":"left" , "address":"COM7" , "ID":"cal_244090"  , "label":"Upstream pressure", "pen_color":"blue"},
       {"module":"USBTC01" , "y_axis":"right" , "address":"Dev2/ai0" , "ID":"cal_244091"  , "label":"Upstream temperature", "pen_color":"black"}]
   }

Notes:
 - refresh_delay is the time period for collection of each data point, in milliseconds
 - left_Y_min, left_Y_max, right_Y_min, and right_Y_max are ranges for viewing the graph during testing
 - left_Y_label and right_Y_label are labels for the axes on the graph.
 - sensor_list is populated with module names found in the "sensors" directory, with arguments as needed for the sensor class.

Each sensor type has a sensor class, which includes a sensor_read() method to return the sensor value. The class accepts property settings as needed for port, address, units, configuration, etc.

Currently supported sensor types are the GP:50 model 612 pressure sensor and National Instrument's USB-TC01 thermocouple interface (Windows only).

When the "Start Data Collection" button is clicked, the dataset is assigned a unique ID. A folder with the ID name is created which holds all files created. A file with the current configuration is written to the folder.

The UI includes a dynamic graph which displays live data collection. Right and left Y axes with separate scales are supported. The range of the graph is manually set with minimum and maximum.

While data is being collected, configuration entry fields in the UI are disabled.

Data collection is logged to a csv file. The data columns in each file will have headers for time (in seconds since epoch) and the sensor label (as defined in the table).

Collection is manually stopped by the user. Pressing "Stop" writes the current file to disk. The configuration file and .csv file are written to a zip file, and an md5 hash is  generated of the .zip to allow data integrity checking.

#### Linux Installation Notes

Clone the python-data-acquisition repository and create a venv folder inside. Initiate a python virtual environment via "python -m venv ./venv"

Activate the virtual environment ("source ./venv/bin/activate") and use pip to install PyQt and PyQtGraph.

xubuntu note: In xubuntu, a number of packages had to be installed via apt before the app would run in python, mainly xcb libraries. Run the python command through strace and look for the "No such file or directory" errors.

The GP:50 612 pressure sensor uses an FTDI FT232R USB to serial converter. However, it is assigned vendor ID 1781, product ID 0bad. This is different than FTDI's ID's, so it is not automatically recognized by the kernel. To attach the sensor to a Virtual COM Port (VCP), do the following:
  - Add the following single line to /etc/udev/rules.d/99-ftdi.rules:
    ACTION=="add", ATTRS{idVendor}=="1781", ATTRS{idProduct}=="0bad", RUN+="/sbin/modprobe ftdi_sio" RUN+="/bin/sh -c 'echo 1781 0bad > /sys/bus/usb-serial/drivers/ftdi_sio/new_id'"
  - Either reboot or run sudo udevadm control --reload to pick up the new rule.
  - Unplug, and plug back in.

Once this is done, you should see something like ttyUSB0 associated with the Mantracourt DSCUSB device via dmesg.

The user must be added to a group with read access to the port (sudo adduser username dialout)

Thermocouple reading should be done with something like the Adafruit MAX31856 amplifier with cold-compensation reference, which communicates over 4-wire SPI and can interface with any K, J, N, R, S, T, E, or B type thermocouple. This can be driven by a PC using the Adafruit FT232H Breakout and the Blinka CircuitPython Compatibility Library (see https://learn.adafruit.com/circuitpython-on-any-computer-with-ft232h/overiew)

***** NOTE ***** While National Instruments does support Linux for some of their devices, support is NOT currently available for their USB devices, including the USB-TC01. The below driver installation notes are included for reference, in case support for their USB devices is added in the future.

Also, as of 9/18/24, NiDAQ drivers are available only for Ubuntu 20.04 and 22.04. This means that NI drivers are not available for Raspberry Pi 5, as only Ubuntu 24.04 is supported on Pi 5. The Pi 4 should work, with Ubuntu 22.04 (I have not tested it though).

To install NI drivers to Ubuntu 22.04:
- Download and install NI Linux Device Drivers from here: https://www.ni.com/en/support/downloads/drivers/download.ni-linux-device-drivers.html#544344 (Do sudo apt install ni-ubuntu2204-drivers-stream.deb, then sudo apt install ni-daqmx) 
- Install the Hardware Configuration Utility from NI for instrument listings, channel designations, etc. (Do sudo apt install ni-hwcfg-utility) 
- In the python virtual environment, install the NiDAQ python library via "python -m pip install nidaqmx".


#### License

GNU General Public License v2

https://gitlab.com/erictmyers/python-data-acquisition.git